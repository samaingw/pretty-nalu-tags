use std::io;

const MAX_USER_ACCESSIBLE_TAGS : usize = 32;
const MAX_NALU_TAGS : usize = 64;

const TAG_NAME: &'static [&'static str; MAX_USER_ACCESSIBLE_TAGS] = &[
    " 1 ", " 2 ", " 3 ", " 4 ", " 5 ", " 6 ", " 7 ", " 8 ", " 9 ", " a ",
    " z ", " e ", " r ", " t ", " y ", " u ", " i ", " o ", " p ", " q ",
    " s ", " d ", " f ", " g ", " h ", " j ", " k ", " l ", " m ", " w ",
    " x ", " c ",
    //, "v", "b", "n",
];

// These are copy-pasted colors from my .Xdefaults config file,
// to match my terminal colors in short.

macro_rules! BLACK {
    () => { "#141311" };
}
macro_rules! BRIGHTBLACK {
    () => { "#4a697d" };
}

macro_rules! RED {
    () => { "#d12f2c" };
}
macro_rules! BRIGHTRED {
    () => { "#fa3935" };
}

macro_rules! GREEN {
    () => { "#819400" };
}
macro_rules! BRIGHTGREEN {
    () => { "#a4bd00" };
}

macro_rules! YELLOW {
    () => { "#b08500" };
}
macro_rules! BRIGHTYELLOW {
    () => { "#d9a400" };
}

macro_rules! BLUE {
    () => { "#2587cc" };
}
macro_rules! BRIGHTBLUE {
    () => { "#2ca2f5" };
}

macro_rules! MAGENTA {
    () => { "#696ebf" };
}
macro_rules! BRIGHTMAGENTA {
    () => { "#8086e8" };
}

macro_rules! CYAN {
    () => { "#289c93" };
}
macro_rules! BRIGHTCYAN {
    () => { "#33c5ba" };
}

macro_rules! WHITE {
    () => { "#bfbaac" };
}
macro_rules! BRIGHTWHITE {
    () => { "#fdf6e3" };
}


// This is ordered by increasing order of importance.
struct Style<'a> {
    default_bg: &'a str,
    default_fg: &'a str,
    empty_tag_bg: &'a str, // neither current view nor any node.
    empty_tag_fg: &'a str,
    filled_tag_bg: &'a str, // one node which is not the focused one has this tag.
    filled_tag_fg: &'a str,
    tag_in_view_bg: &'a str, // the current view has this tag.
    tag_in_view_fg: &'a str,
    tag_in_view_and_filled_bg: &'a str,
    tag_in_view_and_filled_fg: &'a str,
    tag_in_node_bg: &'a str, // the focused node has this tag.
    tag_in_node_fg: &'a str,
    tag_in_both_bg: &'a str, // both the current view and the focused node
    tag_in_both_fg: &'a str, // has this tag.
}

const STYLE : &'static Style = & Style {
    default_bg: "%{B-}",
    default_fg: "%{F-}",
    empty_tag_bg: "%{B-}",
    empty_tag_fg: "%{R}%{B-}",
    filled_tag_bg: "%{B-}",
    filled_tag_fg: concat!("%{F", YELLOW!(), "}"),
    tag_in_view_bg: concat!("%{B", RED!(), "}"),
    tag_in_view_fg: "%{R}%{B-}%{R}", // Take the default background color as foreground while keeping set (red) background color.
    tag_in_view_and_filled_bg: concat!("%{B", GREEN!(), "}"),
    tag_in_view_and_filled_fg: concat!("%{F", BLACK!(), "}"),
    tag_in_node_bg: "%{B-}",
    tag_in_node_fg: concat!("%{F", BRIGHTGREEN!(), "}"),
    tag_in_both_bg: concat!("%{B", BRIGHTGREEN!(), "}"),
    tag_in_both_fg: concat!("%{F", BLACK!(), "}"),
};

fn main() {
    // let do_loop = true;
    let mut line = String::new();
    io::stdin()
        .read_line(&mut line)
        .expect("Failed to read line !");
    let mut len = line.len();
    while len != 0 {
        // println!("line is '{}'", line);
        let view_mark = line.rfind("v").unwrap();
        let node_mark = line.rfind("n").unwrap();
        let  tag_mark = line.rfind("t").unwrap();

        //println!("vmark = {}, nmark = {}, tmark = {}", view_mark, node_mark, tag_mark);


        let   view_tags_str = line.get(view_mark+1..node_mark-1).unwrap();
        let   node_tags_str = line.get(node_mark+1.. tag_mark-1).unwrap();
        let filled_tags_str = line.get(tag_mark+1..len-1).unwrap();

        //println!("vstr = {}, nstr = {}, tstr = {}", view_tags_str, node_tags_str, filled_tags_str);

        let   view_tagset = u64::from_str_radix(view_tags_str, 2)
                                 .expect("Failed to parse to binary !");
        let   node_tagset = u64::from_str_radix(node_tags_str, 2)
                                 .expect("Failed to parse to binary !");
        let filled_tagset = u64::from_str_radix(filled_tags_str, 2)
                                 .expect("Failed to parse to binary !");
        //println!("v = {}, n = {}, t = {}", view_tagset, node_tagset, filled_tagset);

        print!("{} ", line.get(0..view_mark).unwrap());

        for offset in 0..MAX_USER_ACCESSIBLE_TAGS {
            let tag : u64 = 1 << offset;
            let   is_view_tag = (tag & view_tagset) != 0;
            let   is_node_tag = (tag & node_tagset) != 0;
            let is_filled_tag = (tag & filled_tagset) != 0;

            let mut bg = STYLE.empty_tag_bg;
            let mut fg = STYLE.empty_tag_fg;

            if is_view_tag && is_node_tag {
                bg = STYLE.tag_in_both_bg;
                fg = STYLE.tag_in_both_fg;
            } else if is_view_tag && is_filled_tag {
                bg = STYLE.tag_in_view_and_filled_bg;
                fg = STYLE.tag_in_view_and_filled_fg;
            } else if is_view_tag {
                bg = STYLE.tag_in_view_bg;
                fg = STYLE.tag_in_view_fg;
            } else if is_node_tag {
                bg = STYLE.tag_in_node_bg;
                fg = STYLE.tag_in_node_fg;
            } else if is_filled_tag {
                bg = STYLE.filled_tag_bg;
                fg = STYLE.filled_tag_fg;
            }

            print!("{}{}{}",
                      bg,
                      fg,
                      TAG_NAME[offset],
                    );
        }

	if view_tagset >> MAX_USER_ACCESSIBLE_TAGS != 0 {
		// We cannot display thoses tags, but they are present.
		print!(" {}{}non-keyboard bound tags are present", concat!("%{B", RED!(), "}"), STYLE.default_fg)
	}

        println!("{}{}",
                 STYLE.default_bg,
                 STYLE.default_fg,
                );
        //println!("v = {}, n = {}, t = {}", view_tags, node_tags, filled_tags);
        line = String::new();
        io::stdin()
            .read_line(&mut line)
            .expect("Failed to read line !");
        len = line.len();
    }
}
